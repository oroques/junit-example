package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import main.SommeArgent;



public class TestSommeArgent {

	@Test
	public void testAddition() {
		SommeArgent m12CHF = new SommeArgent(12, "CHF");
		SommeArgent m14CHF = new SommeArgent(14, "CHF");
		SommeArgent expected = new SommeArgent(26, "CHF");
		SommeArgent result = m12CHF.add(m14CHF);
		assertTrue(expected.equals(result));
	}

	@Test
	public void testEquivalence() {
		SommeArgent m12CHF = new SommeArgent(12, "CHF");
		SommeArgent m14CHF = new SommeArgent(14, "CHF");
		SommeArgent m14USD = new SommeArgent(14, "USD");

		assertTrue(!m12CHF.equals(null));

		assertEquals(m12CHF, m12CHF);
		assertEquals(m12CHF, new SommeArgent(12, "CHF"));
		assertTrue(!m12CHF.equals(m14CHF));
		assertTrue(!m14USD.equals(m14CHF));

	}
}
