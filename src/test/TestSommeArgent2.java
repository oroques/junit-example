package test;

import static org.junit.Assert.*;

import org.junit.After;

import org.junit.Before;
import org.junit.Test;

import main.SommeArgent;
import main.UniteDistincteException;

public class TestSommeArgent2 {
	private SommeArgent m12CHF;
	private SommeArgent m14CHF;

	@Before
	public void avantTests() {
		System.out.println("avantTests");
		m12CHF = new SommeArgent(12, "CHF");
		m14CHF = new SommeArgent(14, "CHF");
	}

	@After
	public void aprestTests() {
		System.out.println("aprestTests");
	}

	@Test
	public void testAddition() {

		SommeArgent expected = new SommeArgent(26, "CHF");
		SommeArgent result = m12CHF.add(m14CHF);
		assertTrue(expected.equals(result));
	}

	@Test
	public void testAddition2() throws UniteDistincteException {

		SommeArgent expected = new SommeArgent(26, "CHF");
		SommeArgent result = m12CHF.add2(m14CHF);
		assertTrue(expected.equals(result));
	}

	@Test
	public void testEquivalence() {
		SommeArgent m12CHF = new SommeArgent(12, "CHF");
		SommeArgent m14CHF = new SommeArgent(14, "CHF");
		SommeArgent m14USD = new SommeArgent(14, "USD");

		assertTrue(!m12CHF.equals(null));

		assertEquals(m12CHF, m12CHF);
		assertEquals(m12CHF, new SommeArgent(12, "CHF"));
		assertTrue(!m12CHF.equals(m14CHF));
		assertTrue(!m14USD.equals(m14CHF));

	}

	@Test(expected = UniteDistincteException.class)
	public void leveExceptionPourAddition() throws UniteDistincteException {
		SommeArgent autreSomme = new SommeArgent(12, "USD");
		m12CHF.add2(autreSomme);
	}

}
